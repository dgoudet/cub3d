/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bmp.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: daphne <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/14 11:52:50 by daphne            #+#    #+#             */
/*   Updated: 2020/05/14 11:52:55 by daphne           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	init_bmp_variables(t_cub *z)
{
	z->size = 54 + 4 * z->win_width * z->win_height;
	z->zero = 0;
	z->img_start = 54;
	z->header_bytes = 40;
	z->plane = 1;
}

void	write_color_bytes(int fd, t_cub *z)
{
	int x;
	int y;
	int line;

	y = 0;
	while (y < z->win_width)
	{
		x = 0;
		line = z->win_width * (z->win_height - y);
		while (x < z->win_width)
		{
			write(fd, &z->screen_img_data[line * 4], 4);
			line++;
			x++;
		}
		y++;
	}
}

void	save_as_bmp(t_cub *z)
{
	int fd;

	fd = open("screenshot.bmp", O_RDWR | O_CREAT, S_IRWXU | O_TRUNC);
	init_bmp_variables(z);
	write(fd, "BM", 2);
	write(fd, &z->size, sizeof(int));
	write(fd, &z->zero, sizeof(int));
	write(fd, &z->img_start, sizeof(int));
	write(fd, &z->header_bytes, sizeof(int));
	write(fd, &z->win_width, sizeof(int));
	write(fd, &z->win_height, sizeof(int));
	write(fd, &z->plane, sizeof(short int));
	write(fd, &z->bpp, sizeof(short int));
	write(fd, &z->zero, sizeof(int));
	write(fd, &z->zero, sizeof(int));
	write(fd, &z->zero, sizeof(int));
	write(fd, &z->zero, sizeof(int));
	write(fd, &z->zero, sizeof(int));
	write(fd, &z->zero, sizeof(int));
	write_color_bytes(fd, z);
	close(fd);
}
