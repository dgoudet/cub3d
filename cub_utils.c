/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cub_utils.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: daphne <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/09 10:43:09 by daphne            #+#    #+#             */
/*   Updated: 2020/05/09 10:43:52 by daphne           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

size_t		ft_strlen(const char *s)
{
	size_t i;

	i = 0;
	while (s[i])
		i++;
	return (i);
}

char		*ft_strdup(const char *s1)
{
	char	*s2;
	int		i;

	i = 0;
	if ((s2 = malloc(sizeof(*s2) * (ft_strlen(s1) + 1))) == NULL)
		return (NULL);
	while (s1[i])
	{
		s2[i] = s1[i];
		i++;
	}
	s2[i] = '\0';
	return (s2);
}

char		*ft_substr(char const *s, unsigned int start, size_t len)
{
	char			*s2;
	unsigned int	i;
	size_t			size;

	i = 0;
	if (!s)
		return (NULL);
	if (start > ft_strlen(s))
		return (s2 = NULL);
	else
		size = ft_strlen(&s[start]);
	if (size > len)
		size = len;
	if ((s2 = malloc(sizeof(*s2) * (size + 1))) == NULL)
		return (NULL);
	while (i < size)
	{
		s2[i] = s[start];
		i++;
		start++;
	}
	s2[i] = '\0';
	return (s2);
}

void		*ft_memcpy(void *dst, const void *src, size_t n)
{
	size_t				i;
	unsigned char		*d;
	unsigned const char	*s;

	if (!dst && !src)
		return (NULL);
	d = dst;
	s = src;
	i = 0;
	while (i < n)
	{
		d[i] = s[i];
		i++;
	}
	return (dst);
}

char		*ft_strjoin(char const *s1, char const *s2)
{
	char	*s3;
	int		len;
	int		len2;
	int		lent;

	len = 0;
	len2 = 0;
	if (!s1 || !s2)
		return (NULL);
	if (s1)
		len = ft_strlen(s1);
	if (s2)
		len2 = ft_strlen(s2);
	lent = len + len2;
	if ((s3 = malloc(sizeof(*s3) * (lent + 1))) == NULL)
		return (NULL);
	if (s1 && len > 0)
		ft_memcpy(s3, s1, len);
	if (s2 && len2 > 0)
		ft_memcpy(&s3[len], s2, len2);
	s3[lent] = '\0';
	return (s3);
}
