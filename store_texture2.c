/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   store_texture2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dgoudet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/14 12:51:51 by dgoudet           #+#    #+#             */
/*   Updated: 2020/05/14 12:54:21 by daphne           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	calculate_and_store_pixel(char *img_ptr, double wall_y, t_cub *z, int i)
{
	char	*rgb;

	z->img_y = wall_y / z->projected_wall_height * 64;
	rgb = img_ptr + 4 * z->img_x + 4 * z->img_y * z->img_width;
	z->screen_img_data[i] = *rgb;
	rgb = img_ptr + 4 * z->img_x + 4 * z->img_y * z->img_width + 1;
	z->screen_img_data[i + 1] = *rgb;
	rgb = img_ptr + 4 * z->img_x + 4 * z->img_y * z->img_width + 2;
	z->screen_img_data[i + 2] = *rgb;
	z->screen_img_data[i + 3] = 0;
}

void	store_sky(t_cub *z, int *i)
{
	z->win_y = 0;
	while (z->win_y < (z->win_height - z->projected_wall_height) / 2)
	{
		z->screen_img_data[*i] = z->b_sky;
		z->screen_img_data[*i + 1] = z->g_sky;
		z->screen_img_data[*i + 2] = z->r_sky;
		z->screen_img_data[*i + 3] = 0;
		*i = *i + z->win_width * 4;
		z->win_y++;
	}
}

void	store_floor(t_cub *z, int *i)
{
	while (z->win_y <= z->win_height)
	{
		z->screen_img_data[*i] = z->b_floor;
		z->screen_img_data[*i + 1] = z->g_floor;
		z->screen_img_data[*i + 2] = z->r_floor;
		z->screen_img_data[*i + 3] = 0;
		*i = *i + z->win_width * 4;
		z->win_y++;
	}
}

void	store_texture(t_cub *z)
{
	int		i;
	char	*img_ptr;
	double	wall_y;

	i = z->win_x * 4;
	store_sky(z, &i);
	img_ptr = mlx_get_data_addr(z->image, &z->bpp, &z->size_line, &z->endian);
	if (z->projected_wall_height > z->win_height)
		wall_y = (z->projected_wall_height - z->win_height) / 2;
	else
		wall_y = 0;
	while (wall_y < z->projected_wall_height)
	{
		calculate_and_store_pixel(img_ptr, wall_y, z, i);
		wall_y++;
		z->win_y++;
		i = i + z->win_width * 4;
	}
	store_floor(z, &i);
}
