/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_sprite.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dgoudet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/09 18:41:00 by dgoudet           #+#    #+#             */
/*   Updated: 2020/05/09 18:42:49 by daphne           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int	already_stored(t_cub *z, int i, int j)
{
	int k;

	k = 2;
	while (k < z->count_3)
	{
		if (i == z->sprites_info[k] && j == z->sprites_info[k + 1])
			return (1);
		k = k + 5;
	}
	return (0);
}

int	check_sprite_1(t_cub *z, int i, int j)
{
	if (z->map[i][j] == 2 && (!(already_stored(z, i, j))))
	{
		if (z->o == 1)
			dist_to_sprite_1_north(z);
		else if (z->o == 2)
			dist_to_sprite_1_east(z);
		else if (z->o == 3)
			dist_to_sprite_1_south(z);
		else
			dist_to_sprite_1_west(z);
		z->coordinate_i_1 = i;
		z->coordinate_j_1 = j;
		return (1);
	}
	return (0);
}

int	check_sprite_2(t_cub *z, int i, int j)
{
	if (z->map[i][j] == 2 && (!(already_stored(z, i, j))))
	{
		if (z->o == 1)
			dist_to_sprite_2_north(z);
		else if (z->o == 2)
			dist_to_sprite_2_east(z);
		else if (z->o == 3)
			dist_to_sprite_2_south(z);
		else
			dist_to_sprite_2_west(z);
		z->coordinate_i_2 = i;
		z->coordinate_j_2 = j;
		return (1);
	}
	return (0);
}
