/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   store_texture.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: daphne <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/10 20:09:54 by daphne            #+#    #+#             */
/*   Updated: 2020/05/14 12:52:35 by daphne           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int		store_color(char *line, int *i)
{
	int color;

	color = 0;
	while (line[*i] >= '0' && line[*i] <= '9')
	{
		color = color * 10 + (line[*i] - 48);
		(*i)++;
	}
	return (color);
}

void	store_rgb_value(int *r, int *g, int *b, char *line)
{
	int i;

	i = 1;
	while (line[i] == ' ')
		i++;
	*r = store_color(line, &i);
	i++;
	while (line[i] == ' ')
		i++;
	*g = store_color(line, &i);
	i++;
	while (line[i] == ' ')
		i++;
	*b = store_color(line, &i);
}
