/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move_for_and_back.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: daphne <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/09 11:11:30 by daphne            #+#    #+#             */
/*   Updated: 2020/05/10 07:51:45 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	move_forward_config_2(t_cub *z)
{
	if (z->o == 3)
	{
		if (z->angle_start > z->fov / 2)
			z->p_x = z->p_x + sin(z->angle_middle) * z->move_speed;
		else
			z->p_x = z->p_x - sin(z->angle_middle) * z->move_speed;
		z->p_y = z->p_y + cos(z->angle_middle) * z->move_speed;
	}
	else
	{
		z->p_x = z->p_x - cos(z->angle_middle) * z->move_speed;
		if (z->angle_start > (z->fov / z->win_width))
			z->p_y = z->p_y + sin(z->angle_middle) * z->move_speed;
		else
			z->p_y = z->p_y - sin(z->angle_middle) * z->move_speed;
	}
}

void	move_forward_config(t_cub *z)
{
	if (z->dist_to_wall_middle <= 50)
	{
		z->p_x = z->prev_p_x;
		z->p_y = z->prev_p_y;
	}
	else if (z->o == 1)
	{
		if (z->angle_start > z->fov / 2)
			z->p_x = z->p_x - sin(z->angle_middle) * z->move_speed;
		else
			z->p_x = z->p_x + sin(z->angle_middle) * z->move_speed;
		z->p_y = z->p_y - cos(z->angle_middle) * z->move_speed;
	}
	else if (z->o == 2)
	{
		z->p_x = z->p_x + cos(z->angle_middle) * z->move_speed;
		if (z->angle_start > z->fov / 2)
			z->p_y = z->p_y - sin(z->angle_middle) * z->move_speed;
		else
			z->p_y = z->p_y + sin(z->angle_middle) * z->move_speed;
	}
	else
		move_forward_config_2(z);
}

void	move_back_config_2(t_cub *z)
{
	if (z->o == 3)
	{
		if (z->angle_start > z->fov / 2)
			z->p_x = z->p_x - sin(z->angle_middle) * z->move_speed;
		else
			z->p_x = z->p_x + sin(z->angle_middle) * z->move_speed;
		z->p_y = z->p_y - cos(z->angle_middle) * z->move_speed;
		check_for_wall(z);
	}
	else
	{
		z->p_x = z->p_x + cos(z->angle_middle) * z->move_speed;
		if (z->angle_start > (z->fov / z->win_width))
			z->p_y = z->p_y - sin(z->angle_middle) * z->move_speed;
		else
			z->p_y = z->p_y + sin(z->angle_middle) * z->move_speed;
		check_for_wall(z);
	}
}

void	move_back_config(t_cub *z)
{
	if (z->o == 1)
	{
		if (z->angle_start > z->fov / 2)
			z->p_x = z->p_x + sin(z->angle_middle) * z->move_speed;
		else
			z->p_x = z->p_x - sin(z->angle_middle) * z->move_speed;
		z->p_y = z->p_y + cos(z->angle_middle) * z->move_speed;
		check_for_wall(z);
	}
	else if (z->o == 2)
	{
		z->p_x = z->p_x - cos(z->angle_middle) * z->move_speed;
		if (z->angle_start > z->fov / 2)
			z->p_y = z->p_y + sin(z->angle_middle) * z->move_speed;
		else
			z->p_y = z->p_y - sin(z->angle_middle) * z->move_speed;
		check_for_wall(z);
	}
	else
		move_back_config_2(z);
}
