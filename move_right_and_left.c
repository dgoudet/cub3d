/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move_right_and_left.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: daphne <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/09 11:11:54 by daphne            #+#    #+#             */
/*   Updated: 2020/05/10 07:51:56 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	check_for_wall(t_cub *z)
{
	int i;
	int j;

	i = 0;
	j = 0;
	i = z->p_y / z->side;
	j = z->p_x / z->side;
	if (i < 0 || j < 0)
	{
		z->p_x = z->prev_p_x;
		z->p_y = z->prev_p_y;
	}
	else if (z->map[i][j] == 1)
	{
		z->p_x = z->prev_p_x;
		z->p_y = z->prev_p_y;
	}
}

void	move_camera_right_config(t_cub *z)
{
	z->angle_start = z->angle_start -
		((z->fov / z->win_width) * z->rotation_speed);
	if (z->angle_start <= 0)
	{
		z->o = z->o + 1;
		if (z->o > 4)
			z->o = 1;
		z->angle_start = M_PI / 2 - (z->fov / z->win_width);
		z->angle = z->angle_start;
	}
	else
		z->angle = z->angle_start;
}

void	move_camera_left_config(t_cub *z)
{
	z->angle_start = z->angle_start +
		((z->fov / z->win_width) * z->rotation_speed);
	if (z->angle_start >= 1.570796)
	{
		z->o = z->o - 1;
		if (z->o < 1)
			z->o = 4;
		z->angle_start = z->fov / z->win_width;
		z->angle = z->angle_start;
	}
	else
		z->angle = z->angle_start;
}

void	move_right_config(t_cub *z)
{
	if (z->o == 1)
		z->p_x = z->p_x + z->move_speed;
	else if (z->o == 2)
		z->p_y = z->p_y + z->move_speed;
	else if (z->o == 3)
		z->p_x = z->p_x - z->move_speed;
	else
		z->p_y = z->p_y - z->move_speed;
	check_for_wall(z);
}

void	move_left_config(t_cub *z)
{
	if (z->o == 1)
		z->p_x = z->p_x - z->move_speed;
	else if (z->o == 2)
		z->p_y = z->p_y - z->move_speed;
	else if (z->o == 3)
		z->p_x = z->p_x + z->move_speed;
	else
		z->p_y = z->p_y + z->move_speed;
	check_for_wall(z);
}
