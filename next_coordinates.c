/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   next_coordinates.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: daphne <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/09 11:13:14 by daphne            #+#    #+#             */
/*   Updated: 2020/05/09 11:13:16 by daphne           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	calculate_n_ray_y_1(t_cub *z)
{
	if (z->o_precision == 0)
	{
		if (z->o == 1 || z->o == 2)
			z->ray_y_1 = z->ray_y_1 - z->side;
		else
			z->ray_y_1 = z->ray_y_1 + z->side;
	}
	else
	{
		if (z->o == 1 || z->o == 4)
			z->ray_y_1 = z->ray_y_1 - z->side;
		else
			z->ray_y_1 = z->ray_y_1 + z->side;
	}
}

void	calculate_n_ray_x_1(t_cub *z)
{
	if (z->o_precision == 0)
	{
		if (z->o == 1)
			z->ray_x_1 = z->ray_x_1 - z->side * tan(z->angle);
		else if (z->o == 2)
			z->ray_x_1 = z->ray_x_1 + z->side / tan(z->angle);
		else if (z->o == 3)
			z->ray_x_1 = z->ray_x_1 + z->side * tan(z->angle);
		else
			z->ray_x_1 = z->ray_x_1 - z->side / tan(z->angle);
	}
	else
	{
		if (z->o == 1)
			z->ray_x_1 = z->ray_x_1 + z->side * tan(z->angle);
		else if (z->o == 2)
			z->ray_x_1 = z->ray_x_1 + z->side / tan(z->angle);
		else if (z->o == 3)
			z->ray_x_1 = z->ray_x_1 - z->side * tan(z->angle);
		else
			z->ray_x_1 = z->ray_x_1 - z->side / tan(z->angle);
	}
}

void	calculate_n_ray_y_2(t_cub *z)
{
	if (z->o_precision == 0)
	{
		if (z->o == 1)
			z->ray_y_2 = z->ray_y_2 - z->side / tan(z->angle);
		else if (z->o == 2)
			z->ray_y_2 = z->ray_y_2 - z->side * tan(z->angle);
		else if (z->o == 3)
			z->ray_y_2 = z->ray_y_2 + z->side / tan(z->angle);
		else
			z->ray_y_2 = z->ray_y_2 + z->side * tan(z->angle);
	}
	else
	{
		if (z->o == 1)
			z->ray_y_2 = z->ray_y_2 - z->side / tan(z->angle);
		else if (z->o == 2)
			z->ray_y_2 = z->ray_y_2 + z->side * tan(z->angle);
		else if (z->o == 3)
			z->ray_y_2 = z->ray_y_2 + z->side / tan(z->angle);
		else
			z->ray_y_2 = z->ray_y_2 - z->side * tan(z->angle);
	}
}

void	calculate_n_ray_x_2(t_cub *z)
{
	if (z->o_precision == 0)
	{
		if (z->o == 1 || z->o == 4)
			z->ray_x_2 = z->ray_x_2 - z->side;
		else
			z->ray_x_2 = z->ray_x_2 + z->side;
	}
	else
	{
		if (z->o == 1 || z->o == 2)
			z->ray_x_2 = z->ray_x_2 + z->side;
		else
			z->ray_x_2 = z->ray_x_2 - z->side;
	}
}
