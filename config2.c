/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   config2.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: daphne <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/09 10:39:31 by daphne            #+#    #+#             */
/*   Updated: 2020/05/09 10:40:44 by daphne           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int	ft_return_1(int *config_e)
{
	*config_e = 1;
	return (1);
}

int	find_rgb(char *str, t_cub *z, int *i)
{
	int rgb;
	int res;

	rgb = 0;
	res = 0;
	while (str[*i] == ' ')
		(*i)++;
	while (str[*i] >= '0' && str[*i] <= '9')
	{
		rgb = 1;
		res = res * 10 + (str[*i] - 48);
		(*i)++;
	}
	if (rgb == 1 && res >= 0 && res <= 255)
	{
		if (res == 0)
			z->str = ft_strdup("00");
		else
			ft_conv_base(res, "0123456789abcdef", z);
		while (str[*i] == ' ')
			(*i)++;
		return (1);
	}
	else
		return (0);
}

int	config_colour(char *str, t_cub *z, int *config_e)
{
	int		i;
	char	*temp;

	i = 1;
	if (!(find_rgb(str, z, &i)))
		return (0);
	i++;
	z->colour = ft_strdup(z->str);
	free(z->str);
	if (!(find_rgb(str, z, &i)))
		return (0);
	i++;
	temp = ft_strdup(z->colour);
	free(z->colour);
	z->colour = ft_strjoin(temp, z->str);
	free(temp);
	free(z->str);
	if (!(find_rgb(str, z, &i)))
		return (0);
	temp = ft_strdup(z->colour);
	free(z->colour);
	z->colour = ft_strjoin(temp, z->str);
	free(temp);
	free(z->str);
	return (ft_return_1(config_e));
}

int	config_texture(char *str, char **texture, int start, int *config_e)
{
	int	i;
	int	j;

	i = start;
	j = 0;
	while (str[i] == ' ')
		i++;
	while (str[i] && str[i] != ' ')
	{
		i++;
		j++;
	}
	*texture = ft_substr(str, i - j, j);
	while (str[i] == ' ')
		i++;
	if (str[i] == '\0')
		return (ft_return_1(config_e));
	else
	{
		free(*texture);
		return (ft_error_message("Error\nInvalid path for texture"));
	}
}

int	config_resolution(char *str, t_cub *z, int start, int *config_e)
{
	int	i;

	i = start;
	z->win_width = 0;
	z->win_height = 0;
	while (str[i] == ' ')
		i++;
	while (str[i] >= '0' && str[i] <= '9')
	{
		z->win_width = z->win_width * 10 + str[i] - 48;
		i++;
	}
	while (str[i] == ' ')
		i++;
	while (str[i] >= '0' && str[i] <= '9')
	{
		z->win_height = z->win_height * 10 + str[i] - 48;
		i++;
	}
	while (str[i] == ' ')
		i++;
	if (str[i] == '\0' && z->win_width > 0 && z->win_height > 0)
		return (ft_return_1(config_e));
	else
		return (ft_error_message("Error\nInvalid resolution configuration"));
}
