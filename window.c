/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   window.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: daphne <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/07 14:59:43 by daphne            #+#    #+#             */
/*   Updated: 2020/05/14 12:29:28 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	win_init(t_cub *z, int argc)
{
	int	size_x;
	int	size_y;

	z->mlx = mlx_init();
	mlx_get_screen_size(z->mlx, &size_x, &size_y);
	if (z->win_width > size_x)
		z->win_width = size_x;
	if (z->win_height > size_y)
		z->win_height = size_y;
	if (argc != 3)
		z->win = mlx_new_window(z->mlx, z->win_width, z->win_height, "Cub3D");
}
