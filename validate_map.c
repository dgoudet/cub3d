/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   validate_map.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: daphne <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/09 11:22:38 by daphne            #+#    #+#             */
/*   Updated: 2020/05/09 11:23:25 by daphne           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int	check_angle_2(t_cub *z, int x, int y)
{
	if (x > 0 && y > 0)
	{
		if (z->map[x - 1][y - 1] != 1 && z->map[x - 1][y - 1] != -2)
			return (0);
	}
	if (x > 0 && y < z->map_y - 1)
	{
		if (z->map[x - 1][y + 1] != 1 && z->map[x - 1][y + 1] != -2)
			return (0);
	}
	if (x < z->map_x - 1 && y > 0)
	{
		if (z->map[x + 1][y - 1] != 1 && z->map[x + 1][y - 1] != -2)
			return (0);
	}
	if (x < z->map_x - 1 && y < z->map_y - 1)
	{
		if (z->map[x + 1][y + 1] != 1 && z->map[x + 1][y + 1] != -2)
			return (0);
	}
	return (1);
}

int	check_angle(t_cub *z, int x, int y)
{
	if (x > 0)
	{
		if (z->map[x - 1][y] != 1 && z->map[x - 1][y] != -2)
			return (0);
	}
	if (y > 0)
	{
		if (z->map[x][y - 1] != 1 && z->map[x][y - 1] != -2)
			return (0);
	}
	if (x < z->map_x - 1)
	{
		if (z->map[x + 1][y] != 1 && z->map[x + 1][y] != -2)
			return (0);
	}
	if (y < z->map_y - 1)
	{
		if (z->map[x][y + 1] != 1 && z->map[x][y + 1] != -2)
			return (0);
	}
	return (check_angle_2(z, x, y));
}

int	check_walls(t_cub *z, int x, int y)
{
	int temp;

	temp = x;
	while (temp > 0 && z->map[temp][y] != 1)
		temp--;
	if (z->map[temp][y] != 1)
		return (0);
	temp = x;
	while (temp < z->map_x - 1 && z->map[temp][y] != 1)
		temp++;
	if (z->map[temp][y] != 1)
		return (0);
	temp = y;
	while (temp > 0 && z->map[x][temp] != 1)
		temp--;
	if (z->map[x][temp] != 1)
		return (0);
	temp = y;
	while (temp < z->map_y - 1 && z->map[x][temp] != 1)
		temp++;
	if (z->map[x][temp] != 1)
		return (0);
	return (1);
}

int	check_content(t_cub *z)
{
	int o;
	int x;
	int y;

	o = 0;
	x = 0;
	y = 0;
	while (x < z->map_x)
	{
		while (z->map[x][y])
		{
			if (z->map[x][y] >= 3 && z->map[x][y] <= 6)
				o++;
			if (z->map[x][y] < -2 || z->map[x][y] > 6)
				return (0);
			y++;
		}
		y = 0;
		x++;
	}
	if (o != 1)
		return (0);
	return (1);
}

int	validate_map(t_cub *z)
{
	int x;
	int y;

	x = 0;
	y = 0;
	while (x < z->map_x)
	{
		while (z->map[x][y])
		{
			if (z->map[x][y] == -2)
			{
				if (!(check_angle(z, x, y)))
					return (0);
			}
			if (z->map[x][y] == -1)
			{
				if (!(check_walls(z, x, y)))
					return (0);
			}
			y++;
		}
		y = 0;
		x++;
	}
	return (check_content(z));
}
