/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   texture.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: daphne <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/09 11:18:04 by daphne            #+#    #+#             */
/*   Updated: 2020/05/12 19:43:23 by daphne           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	texture_config_1(t_cub *z)
{
	double	d;

	d = z->ray_y_1 / 64;
	while (d >= 1)
		d = d - 1;
	if (d <= 0.5)
		z->image = z->img_n;
	else
		z->image = z->img_s;
	d = z->ray_x_1 / z->img_width;
	while (d > 1)
		d = d - 1;
	z->img_x = d * z->img_width;
}

void	texture_config_2(t_cub *z)
{
	double	d;

	d = z->ray_x_2 / z->side;
	while (d >= 1)
		d = d - 1;
	if (d >= 0.5)
		z->image = z->img_e;
	else
		z->image = z->img_w;
	d = z->ray_y_2 / z->img_width;
	while (d > 1)
		d = d - 1;
	z->img_x = d * z->img_width;
}

void	rgb_to_hexa(t_cub *z)
{
	char	*temp;

	if (*z->rgb == 0)
		z->str = ft_strdup("00");
	else
		ft_conv_base(*z->rgb, "0123456789abcdef", z);
	if (ft_strlen(z->str) == 1)
	{
		temp = ft_strdup(z->str);
		free(z->str);
		z->str = ft_strjoin("0", temp);
		free(temp);
	}
}

void	calculate_and_draw_pixel_colour(void *img_ptr, double wall_y, t_cub *z)
{
	char			*temp;
	unsigned int	colour;

	z->img_y = wall_y / z->projected_wall_height * 64;
	z->rgb = img_ptr + 4 * z->img_x + 4 * z->img_y * z->img_width + 2;
	rgb_to_hexa(z);
	z->img_colour = ft_strdup(z->str);
	free(z->str);
	z->rgb = img_ptr + 4 * z->img_x + 4 * z->img_y * z->img_width + 1;
	rgb_to_hexa(z);
	temp = ft_strdup(z->img_colour);
	free(z->img_colour);
	z->img_colour = ft_strjoin(temp, z->str);
	free(z->str);
	free(temp);
	z->rgb = img_ptr + 4 * z->img_x + 4 * z->img_y * z->img_width;
	rgb_to_hexa(z);
	temp = ft_strdup(z->img_colour);
	free(z->img_colour);
	z->img_colour = ft_strjoin(temp, z->str);
	free(z->str);
	free(temp);
	colour = ft_hexatoi(z->img_colour);
	mlx_pixel_put(z->mlx, z->win, z->win_x, z->win_y, colour);
	free(z->img_colour);
}

void	put_texture(t_cub *z)
{
	void	*img_ptr;
	double	wall_y;

	z->win_y = 0;
	while (z->win_y < (z->win_height - z->projected_wall_height) / 2)
	{
		mlx_pixel_put(z->mlx, z->win, z->win_x, z->win_y, z->colour_s);
		z->win_y++;
	}
	img_ptr = mlx_get_data_addr(z->image, &z->bpp, &z->size_line, &z->endian);
	if (z->projected_wall_height > z->win_height)
		wall_y = (z->projected_wall_height - z->win_height) / 2;
	else
		wall_y = 0;
	while (wall_y < z->projected_wall_height)
	{
		calculate_and_draw_pixel_colour(img_ptr, wall_y, z);
		wall_y++;
		z->win_y++;
	}
	while (z->win_y <= z->win_height)
	{
		mlx_pixel_put(z->mlx, z->win, z->win_x, z->win_y, z->colour_f);
		z->win_y++;
	}
}
