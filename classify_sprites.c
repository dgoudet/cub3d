/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   classify_sprites.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: daphne <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/09 11:36:13 by daphne            #+#    #+#             */
/*   Updated: 2020/05/10 08:05:23 by daphne           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	classify_sprites3(t_cub *z, int *temp, int *i)
{
	while (*i < z->count_3)
	{
		z->sprites_info[*i] = temp[*i];
		(*i)++;
	}
	*i = 0;
	free(temp);
	temp = NULL;
}

void	classify_sprites2(t_cub *z, int *temp, int *i)
{
	int	j;
	int k;

	j = 0;
	k = 0;
	while (j < *i)
	{
		z->sprites_info[j] = temp[j];
		j++;
	}
	while (k < 5)
	{
		z->sprites_info[*i] = temp[*i + 5];
		(*i)++;
		k++;
	}
	k = 0;
	while (k < 5)
	{
		z->sprites_info[*i] = temp[j];
		(*i)++;
		j++;
		k++;
	}
	classify_sprites3(z, temp, i);
}

void	classify_sprites(t_cub *z)
{
	int	*temp;
	int	i;
	int	j;

	i = 0;
	while (i < z->count_3 - 5)
	{
		j = 0;
		if (z->sprites_info[i] < z->sprites_info[i + 5])
		{
			if (!(temp = malloc(sizeof(int) * z->count_3 + 1)))
				return ;
			while (j < z->count_3)
			{
				temp[j] = z->sprites_info[j];
				j++;
			}
			free(z->sprites_info);
			if (!(z->sprites_info = malloc(sizeof(int) * z->count_3 + 1)))
				return ;
			classify_sprites2(z, temp, &i);
		}
		else
			i = i + 5;
	}
}
