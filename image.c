/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   image.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: daphne <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/09 11:06:30 by daphne            #+#    #+#             */
/*   Updated: 2020/05/14 11:55:26 by daphne           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int		ft_image_error(char *str)
{
	errno = 22;
	perror(str);
	return (0);
}

void	*img_create(t_cub *z, char *path)
{
	void	*image;

	z->img_width = 64;
	z->img_height = 64;
	z->sprite_width = 64;
	z->sprite_height = 64;
	z->size_line_sprite = z->sprite_width * 4;
	z->bpp = 32;
	z->size_line = z->img_width * 4;
	z->endian = 0;
	image = mlx_xpm_file_to_image(z->mlx, path, &z->img_width, &z->img_height);
	return (image);
}

void	img_screen_ptr(t_cub *z)
{
	int	size_line;

	size_line = z->win_width * 4;
	z->screen_img = mlx_new_image(z->mlx, z->win_width, z->win_height);
	z->screen_img_data =
		mlx_get_data_addr(z->screen_img, &z->bpp, &size_line, &z->endian);
}

void	ft_free_texture_ptr(t_cub *z)
{
	z->texture_n = NULL;
	z->texture_e = NULL;
	z->texture_s = NULL;
	z->texture_w = NULL;
	z->texture_sprite = NULL;
	free(z->texture_sprite);
	free(z->texture_n);
	free(z->texture_e);
	free(z->texture_s);
	free(z->texture_w);
	z->texture_n = NULL;
	z->texture_e = NULL;
	z->texture_s = NULL;
	z->texture_w = NULL;
	z->texture_sprite = NULL;
}

int		img_init(t_cub *z)
{
	z->img_n = img_create(z, z->texture_n);
	if (z->img_n == NULL)
		return (ft_image_error("Error\nInvalid path for NORTH texture"));
	z->img_s = img_create(z, z->texture_s);
	if (z->img_s == NULL)
		return (ft_image_error("Error\nInvalid path for SOUTH texture"));
	z->img_e = img_create(z, z->texture_e);
	if (z->img_e == NULL)
		return (ft_image_error("Error\nInvalid path for EAST texture"));
	z->img_w = img_create(z, z->texture_w);
	if (z->img_w == NULL)
		return (ft_image_error("Error\nInvalid path for WEST texture"));
	z->img_sprite = img_create(z, z->texture_sprite);
	if (z->img_sprite == NULL)
		return (ft_image_error("Error\nInvalid path for SPRITE texture"));
	ft_free_texture_ptr(z);
	return (1);
}
