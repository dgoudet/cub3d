/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_sprite.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dgoudet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/09 12:50:37 by dgoudet           #+#    #+#             */
/*   Updated: 2020/05/22 13:09:46 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	store_sprite_pixel(void *img_ptr, double sprite_y, t_cub *z)
{
	char	*rgb;
	int		i;

	i = z->win_x * 4 + z->win_width * 4 * z->win_y;
	z->img_y = sprite_y / z->projected_sprite_height * 64;
	rgb = img_ptr + 4 * z->img_x + 4 * z->img_y * z->sprite_width;
	z->screen_img_data[i] = *rgb;
	rgb = img_ptr + 4 * z->img_x + 4 * z->img_y * z->sprite_width + 1;
	z->screen_img_data[i + 1] = *rgb;
	rgb = img_ptr + 4 * z->img_x + 4 * z->img_y * z->sprite_width + 2;
	z->screen_img_data[i + 2] = *rgb;
	z->screen_img_data[i + 3] = 0;
}

int		is_color(void *img_ptr, double sprite_y, t_cub *z)
{
	unsigned int	colour;

	colour = determine_color(img_ptr, sprite_y, z);
	free(z->img_colour);
	if (colour != 0)
		return (1);
	return (0);
}

void	draw_sprite(int i, double sprite_x, double sprite_y, t_cub *z)
{
	void	*img_ptr;

	img_ptr = mlx_get_data_addr(z->img_sprite, &z->bpp,
			&z->size_line_sprite, &z->endian);
	while (sprite_x < z->projected_sprite_width)
	{
		z->win_y = (z->win_height - z->projected_sprite_height) / 2 +
			z->projected_sprite_height / 2;
		while (sprite_y < z->projected_sprite_height
				&& z->sprites_info[i - 4] < z->stored_distances[z->win_x])
		{
			if (z->save == 1 && is_color(img_ptr, sprite_y, z))
				store_sprite_pixel(img_ptr, sprite_y, z);
			else
				draw_sprite_pixel(img_ptr, sprite_y, z);
			sprite_y++;
			z->win_y++;
		}
		sprite_y = 0;
		sprite_x++;
		z->win_x++;
		z->img_x = sprite_x / z->projected_sprite_width * 64;
	}
	z->win_x = 0;
}

void	draw_sprite_pixel(void *img_ptr, double sprite_y, t_cub *z)
{
	unsigned int	colour;

	colour = determine_color(img_ptr, sprite_y, z);
	if (colour != 0)
		mlx_pixel_put(z->mlx, z->win, z->win_x, z->win_y, colour);
	free(z->img_colour);
}

void	config_sprite_and_draw(t_cub *z, int i)
{
	double	sprite_y;
	double	sprite_x;

	z->projected_sprite_height = z->sprite_height *
		z->dist_to_proj_plane / z->sprites_info[i - 4] / 2;
	z->projected_sprite_width = z->sprite_width *
		z->dist_to_proj_plane / z->sprites_info[i - 4] / 2;
	z->win_y = (z->win_height - z->projected_sprite_height);
	if (z->projected_sprite_height > z->win_height)
		sprite_y = (z->projected_sprite_height - z->win_height);
	else
		sprite_y = 0;
	if (z->projected_sprite_width > z->win_width)
		sprite_x = (z->projected_sprite_width - z->win_width);
	else
		sprite_x = z->sprites_info[i - 3] *
			z->projected_sprite_width / z->sprite_width;
	z->img_x = z->sprites_info[i - 3];
	draw_sprite(i, sprite_x, sprite_y, z);
}
