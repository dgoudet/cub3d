/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   config.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: daphne <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/09 10:30:41 by daphne            #+#    #+#             */
/*   Updated: 2020/05/14 14:02:30 by daphne           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int		ft_error_message(char *str)
{
	errno = 22;
	perror(str);
	return (0);
}

int		parse2(char *str, t_cub *z)
{
	if (str[0] == 'C' && z->c == 0)
	{
		if (!(config_colour(str, z, &z->c)))
			return (ft_error_message("Error\nInvalid CEILING colour"));
		z->colour_s = ft_hexatoi(z->colour);
		free(z->colour);
		return (1);
	}
	else if ((str[0] == ' ' || str[0] == '1')
			&& z->r == 1 && z->no == 1 && z->so == 1
			&& z->ea == 1 && z->we == 1 && z->s == 1 && z->f == 1 && z->c == 1)
	{
		if (z->map_y < (int)ft_strlen(str))
			z->map_y = (int)ft_strlen(str);
		if (!(make_map(str, z)))
			return (0);
		return (1);
	}
	else if (str[0] == '\0')
		return (1);
	return (ft_error_message("Error\nInvalid argument list"));
}

int		parse(char *str, t_cub *z)
{
	errno = 22;
	if (str[0] == 'R' && z->r == 0)
		return (config_resolution(str, z, 1, &z->r));
	else if ((ft_strncmp(str, "NO", 2) && z->no == 0))
		return (config_texture(str, &z->texture_n, 2, &z->no));
	else if ((ft_strncmp(str, "SO", 2) && z->so == 0))
		return (config_texture(str, &z->texture_s, 2, &z->so));
	else if ((ft_strncmp(str, "EA", 2) && z->ea == 0))
		return (config_texture(str, &z->texture_e, 2, &z->ea));
	else if ((ft_strncmp(str, "WE", 2) && z->we == 0))
		return (config_texture(str, &z->texture_w, 2, &z->we));
	else if (str[0] == 'S' && z->s == 0)
		return (config_texture(str, &z->texture_sprite, 1, &z->s));
	else if (str[0] == 'F' && z->f == 0)
	{
		if (!(config_colour(str, z, &z->f)))
			return (ft_error_message("Error\nInvalid FLOOR colour"));
		z->colour_f = ft_hexatoi(z->colour);
		free(z->colour);
		return (1);
	}
	else
		return (parse2(str, z));
}

void	init_variables(t_cub *z)
{
	z->r = 0;
	z->no = 0;
	z->so = 0;
	z->ea = 0;
	z->we = 0;
	z->s = 0;
	z->f = 0;
	z->c = 0;
	z->map = NULL;
	z->map_x = 0;
	z->map_y = 0;
	z->r_floor = 0;
	z->r_sky = 0;
	z->g_floor = 0;
	z->g_sky = 0;
	z->b_floor = 0;
	z->b_sky = 0;
	z->texture_n = NULL;
	z->texture_e = NULL;
	z->texture_w = NULL;
	z->texture_s = NULL;
	z->texture_sprite = NULL;
}

int		read_and_setup_config(char *file, t_cub *z)
{
	char	*line;
	int		gnl;
	int		fd;

	gnl = 1;
	init_variables(z);
	if ((fd = open(file, O_RDONLY)) < 0 || !(is_correct_file(file)))
		return (ft_error_message("Error\nWrong format or unreadable file"));
	fd = open(file, O_RDONLY);
	while (gnl == 1)
	{
		gnl = get_next_line(fd, &line);
		if (!(parse(line, z)))
		{
			free(line);
			return (0);
		}
		if (line[0] == 'F')
			store_rgb_value(&z->r_floor, &z->g_floor, &z->b_floor, line);
		if (line[0] == 'C')
			store_rgb_value(&z->r_sky, &z->g_sky, &z->b_sky, line);
		free(line);
	}
	close(fd);
	return (read_and_setup_config2(z));
}
