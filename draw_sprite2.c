/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_sprite2.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: daphne <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/22 13:05:11 by daphne            #+#    #+#             */
/*   Updated: 2020/05/22 13:08:41 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

unsigned int	determine_color(void *img_ptr, double sprite_y, t_cub *z)
{
	unsigned int	colour;
	char			*temp;

	z->img_y = sprite_y / z->projected_sprite_height * 64;
	z->rgb = img_ptr + 4 * z->img_x + 4 * z->img_y * z->sprite_width + 2;
	rgb_to_hexa(z);
	z->img_colour = ft_strdup(z->str);
	free(z->str);
	z->rgb = img_ptr + 4 * z->img_x + 4 * z->img_y * z->sprite_width + 1;
	rgb_to_hexa(z);
	temp = ft_strdup(z->img_colour);
	free(z->img_colour);
	z->img_colour = ft_strjoin(temp, z->str);
	free(z->str);
	free(temp);
	z->rgb = img_ptr + 4 * z->img_x + 4 * z->img_y * z->sprite_width;
	rgb_to_hexa(z);
	temp = ft_strdup(z->img_colour);
	free(z->img_colour);
	z->img_colour = ft_strjoin(temp, z->str);
	free(z->str);
	free(temp);
	return (colour = ft_hexatoi(z->img_colour));
}
