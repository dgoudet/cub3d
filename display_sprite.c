/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display_sprite.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: daphne <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/09 11:36:13 by daphne            #+#    #+#             */
/*   Updated: 2020/05/09 13:37:48 by daphne           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	store_sprite_info2(t_cub *z)
{
	int ret_1;
	int ret_2;

	ret_1 = 0;
	ret_2 = 0;
	while (ret_1 == 0 || ret_2 == 0)
	{
		z->dist_to_sprite_1 = -1;
		z->dist_to_sprite_2 = -1;
		ret_1 = calc_sprite_hor_intersection(z);
		ret_2 = calc_sprite_ver_intersection(z);
		if (ret_1 == 0 || ret_2 == 0)
			calculate_dist_to_sprite(z);
	}
}

void	store_sprite_info(t_cub *z)
{
	if (!(z->sprites_info = malloc(sizeof(int) * z->win_width)))
		return ;
	z->count_3 = 0;
	z->o_precision = 0;
	z->angle = z->angle_start;
	z->angle_from_middle = z->fov / 2;
	z->win_x = 0;
	while (z->win_x <= z->win_width)
	{
		store_sprite_info2(z);
		z->win_x++;
		if (z->o_precision == 0)
		{
			z->angle = z->angle - (z->fov / z->win_width);
			if (z->angle <= 0.002014)
				z->o_precision = 1;
		}
		else
			z->angle = z->angle + (z->fov / z->win_width);
		if (z->win_x < z->win_width / 2)
			z->angle_from_middle = z->angle_from_middle - z->fov / z->win_width;
		else
			z->angle_from_middle = z->angle_from_middle + z->fov / z->win_width;
	}
}

void	display_sprites(t_cub *z)
{
	int i;

	store_sprite_info(z);
	if (z->count_3 > 4)
		classify_sprites(z);
	z->angle = z->angle_start;
	z->o_precision = 0;
	z->win_x = 0;
	i = 4;
	while (z->win_x <= z->win_width && z->count_3 != 0 && i < z->count_3)
	{
		while (z->win_x <= z->win_width && z->win_x != z->sprites_info[i]
				&& z->sprites_info[i] != 0)
			z->win_x++;
		if (z->win_x == z->sprites_info[i] || z->sprites_info[i] == 0)
			config_sprite_and_draw(z, i);
		i = i + 5;
		z->win_x++;
	}
	free(z->sprites_info);
	free(z->stored_distances);
}
