/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   config3.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: daphne <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/15 12:34:34 by daphne            #+#    #+#             */
/*   Updated: 2020/05/15 12:34:51 by daphne           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int	is_correct_file(char *file)
{
	int	i;

	i = 0;
	while (file[i] != '.')
		i++;
	i++;
	if (file[i] == 'c')
	{
		i++;
		if (file[i] == 'u')
		{
			i++;
			if (file[i] == 'b')
			{
				i++;
				if (file[i] == '\0')
					return (1);
			}
		}
	}
	return (0);
}

int	read_and_setup_config2(t_cub *z)
{
	if (!(validate_map(z)))
		return (ft_error_message("Error\nInvalid map"));
	return (1);
}
