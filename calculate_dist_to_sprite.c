/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   calculate_dist_to_sprite.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: daphne <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/09 15:59:09 by daphne            #+#    #+#             */
/*   Updated: 2020/05/09 18:41:35 by daphne           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	config_dist_to_sprite_1(t_cub *z)
{
	double	d;

	z->sprites_info[z->count_3] = z->dist_to_sprite_1;
	z->count_3++;
	if (z->o == 1 || z->o == 3)
		d = z->ray_x_1 / z->sprite_width;
	else
		d = z->ray_y_1 / z->sprite_width;
	while (d >= 1)
		d = d - 1;
	if (z->o == 1 || z->o == 2)
		z->sprites_info[z->count_3] = d * z->sprite_width;
	else
		z->sprites_info[z->count_3] = (1 - d) * z->sprite_width;
	z->count_3++;
	z->sprites_info[z->count_3] = z->coordinate_i_1;
	z->count_3++;
	z->sprites_info[z->count_3] = z->coordinate_j_1;
	z->count_3++;
	z->sprites_info[z->count_3] = z->win_x;
	z->count_3++;
}

void	config_dist_to_sprite_2(t_cub *z)
{
	double	d;

	z->sprites_info[z->count_3] = z->dist_to_sprite_2;
	z->count_3++;
	if (z->o == 1 || z->o == 3)
		d = z->ray_x_2 / z->sprite_width;
	else
		d = z->ray_y_2 / z->sprite_width;
	while (d >= 1)
		d = d - 1;
	if (z->o == 1 || z->o == 2)
		z->sprites_info[z->count_3] = d * z->sprite_width;
	else
		z->sprites_info[z->count_3] = (1 - d) * z->sprite_width;
	z->count_3++;
	z->sprites_info[z->count_3] = z->coordinate_i_2;
	z->count_3++;
	z->sprites_info[z->count_3] = z->coordinate_j_2;
	z->count_3++;
	z->sprites_info[z->count_3] = z->win_x;
	z->count_3++;
}

void	calculate_dist_to_sprite(t_cub *z)
{
	if (z->dist_to_sprite_1 < 0 && z->dist_to_sprite_2 < 0)
		return ;
	if (z->dist_to_sprite_1 < 0)
		config_dist_to_sprite_2(z);
	else if (z->dist_to_sprite_2 < 0)
		config_dist_to_sprite_1(z);
	else if (z->dist_to_sprite_1 < z->dist_to_sprite_2)
		config_dist_to_sprite_1(z);
	else
		config_dist_to_sprite_2(z);
}
