/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   calculate_sprite_intersection.c                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dgoudet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/08 19:51:43 by dgoudet           #+#    #+#             */
/*   Updated: 2020/05/09 10:28:38 by daphne           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int		calc_sprite_hor_intersection2(t_cub *z, int i, int j)
{
	while (z->map[i][j] != 1
			&& (z->ray_y_1 >= 0 && z->ray_y_1 <= z->ray_y_max)
			&& (z->ray_x_1 >= 0 && z->ray_x_1 <= z->ray_x_max))
	{
		calculate_n_ray_y_1(z);
		calculate_n_ray_x_1(z);
		if ((z->ray_y_1 > 0 && z->ray_y_1 < z->ray_y_max)
				&& (z->ray_x_1 > 0 && z->ray_x_1 < z->ray_x_max))
		{
			i = z->ray_y_1 / z->side;
			j = z->ray_x_1 / z->side;
			if (check_sprite_1(z, i, j))
				return (0);
		}
	}
	return (1);
}

int		calc_sprite_hor_intersection(t_cub *z)
{
	int	i;
	int	j;

	i = 0;
	j = 0;
	calculate_ray_y_1(z);
	calculate_ray_x_1(z);
	i = z->ray_y_1 / z->side;
	j = z->ray_x_1 / z->side;
	if (z->ray_y_1 < 0 || z->ray_x_1 < 0 || z->ray_y_1 >= z->ray_y_max
			|| z->ray_x_1 >= z->ray_x_max)
		return (1);
	if (check_sprite_1(z, i, j))
		return (0);
	if ((z->ray_y_1 >= 0 && z->ray_y_1 <= z->ray_y_max)
			&& (z->ray_x_1 >= 0 && z->ray_x_1 <= z->ray_x_max))
		return (calc_sprite_hor_intersection2(z, i, j));
	return (1);
}

int		calc_sprite_ver_intersection2(t_cub *z, int i, int j)
{
	while (z->map[i][j] != 1
			&& (z->ray_y_2 >= 0 && z->ray_y_2 <= z->ray_y_max)
			&& (z->ray_x_2 >= 0 && z->ray_x_2 <= z->ray_x_max))
	{
		calculate_n_ray_x_2(z);
		calculate_n_ray_y_2(z);
		if ((z->ray_y_2 > 0 && z->ray_y_2 < z->ray_y_max)
				&& (z->ray_x_2 > 0 && z->ray_x_2 < z->ray_x_max))
		{
			i = z->ray_y_2 / z->side;
			j = z->ray_x_2 / z->side;
			if (check_sprite_2(z, i, j))
				return (0);
		}
	}
	return (1);
}

int		calc_sprite_ver_intersection(t_cub *z)
{
	int	i;
	int	j;

	calculate_ray_x_2(z);
	calculate_ray_y_2(z);
	i = z->ray_y_2 / z->side;
	j = z->ray_x_2 / z->side;
	if (z->ray_y_2 < 0 || z->ray_x_2 < 0 ||
			z->ray_y_2 >= z->ray_y_max || z->ray_x_2 >= z->ray_x_max)
		return (1);
	if (check_sprite_2(z, i, j))
		return (0);
	if ((z->ray_y_2 >= 0 && z->ray_y_2 <= z->ray_y_max)
			&& (z->ray_x_2 >= 0 && z->ray_x_2 <= z->ray_x_max))
		return (calc_sprite_ver_intersection2(z, i, j));
	return (1);
}
