/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cub3d.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: daphne <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/09 18:49:00 by daphne            #+#    #+#             */
/*   Updated: 2020/05/22 13:11:11 by daphne           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CUB3D_H
# define CUB3D_H
# include <stdio.h>
# include "mlx.h"
# include <unistd.h>
# include <string.h>
# include <stdlib.h>
# include <math.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>
# include <errno.h>

# define KEYPRESS_EVENT 2
# define KEYPRESS_MASK 1
# define KEYRELEASE_EVENT 3
# define KEYRELEASE_MASK 2

typedef struct		s_cub
{
	int				save;
	void			*mlx;
	void			*win;
	void			*screen_img;
	char			*screen_img_data;
	int				r_floor;
	int				r_sky;
	int				g_floor;
	int				g_sky;
	int				b_floor;
	int				b_sky;
	int				win_x;
	int				win_y;
	int				win_height;
	int				win_width;
	char			*img_n;
	char			*img_s;
	char			*img_e;
	char			*img_w;
	char			*img_sprite;
	int				img_x;
	int				img_y;
	int				img_width;
	int				img_height;
	int				sprite_width;
	int				sprite_height;
	int				bpp;
	int				size_line;
	int				endian;
	void			*image;
	int				prev_dist;
	char			*str;
	int				count;
	int				count_2;
	char			*img_colour;
	unsigned char	*rgb;
	int				o;
	int				o_precision;
	int				dist_to_proj_plane;
	int				p_x;
	int				p_y;
	int				prev_p_x;
	int				prev_p_y;
	long double		fov;
	long double		angle;
	long double		fov_angle;
	long double		angle_start;
	long double		angle_middle;
	long double		angle_from_middle;
	int				side;
	double			ray_x_1;
	double			ray_y_1;
	double			ray_x_2;
	double			ray_y_2;
	int				ray_y_max;
	int				ray_x_max;
	int				dist_to_wall;
	int				dist_to_wall_middle;
	int				dist_to_wall_1;
	int				dist_to_wall_2;
	int				wall_height;
	int				projected_wall_height;
	int				*stored_distances;
	int				move_camera_right;
	int				move_camera_left;
	int				move_forward;
	int				move_back;
	int				move_right;
	int				move_left;
	int				move_speed;
	int				rotation_speed;
	int				exit;
	int				r;
	int				no;
	int				so;
	int				we;
	int				ea;
	int				s;
	int				f;
	int				c;
	char			*colour;
	char			*texture_n;
	char			*texture_s;
	char			*texture_e;
	char			*texture_w;
	char			*texture_sprite;
	int				colour_f;
	int				colour_s;
	int				**map;
	int				map_x;
	int				map_y;
	int				*sprites_info;
	int				dist_to_sprite_1;
	int				dist_to_sprite_2;
	int				coordinate_i_1;
	int				coordinate_i_2;
	int				coordinate_j_1;
	int				coordinate_j_2;
	int				projected_sprite_height;
	int				projected_sprite_width;
	int				size_line_sprite;
	int				count_3;
	int				size;
	int				zero;
	int				img_start;
	int				header_bytes;
	int				plane;
}					t_cub;

void				ft_conv_base(unsigned long nb, char *base, t_cub *z);
int					ft_hexatoi(char *str);
size_t				ft_strlen(const char *s);
char				*ft_substr(char const *s, unsigned int start, size_t len);
char				*ft_strjoin(char const *s1, char const *s2);
char				*ft_strdup(const char *s1);
int					ft_strncmp(char *s1, char *s2, size_t n);
void				put_texture(t_cub *z);
void				win_init(t_cub *z, int argc);
int					img_init(t_cub *z);
void				game_init(t_cub *z, int argc);
void				calculate_ray_y_1(t_cub *z);
void				calculate_ray_x_1(t_cub *z);
void				calculate_ray_y_2(t_cub *z);
void				calculate_ray_x_2(t_cub *z);
void				calculate_n_ray_y_1(t_cub *z);
void				calculate_n_ray_x_1(t_cub *z);
void				calculate_n_ray_y_2(t_cub *z);
void				calculate_n_ray_x_2(t_cub *z);
void				calculate_dist_to_wall_1(t_cub *z);
void				calculate_dist_to_wall_2(t_cub *z);
void				calculate_dist_to_wall(t_cub *z);
void				texture_config_1(t_cub *z);
void				texture_config_2(t_cub *z);
void				calculate_horizontal_intersection(t_cub *z);
void				calculate_vertical_intersection(t_cub *z);
int					move(t_cub *z);
void				check_for_wall(t_cub *z);
void				move_camera_right_config(t_cub *z);
void				move_camera_left_config(t_cub *z);
void				move_forward_config(t_cub *z);
void				move_back_config(t_cub *z);
void				move_right_config(t_cub *z);
void				move_left_config(t_cub *z);
int					exit_program(t_cub *z);
int					read_and_setup_config(char *file, t_cub *z);
int					ft_error_message(char *str);
int					config_colour(char *str, t_cub *z, int *config_e);
int					config_texture(char *str, char **texture,
		int start, int *config_e);
int					config_resolution(char *str, t_cub *z,
		int start, int *config_e);
int					make_map(char *str, t_cub *z);
int					validate_map(t_cub *z);
int					get_next_line(int fd, char **line);
void				calculate_dist_to_sprite(t_cub *z);
int					check_sprite_1(t_cub *z, int i, int j);
int					check_sprite_2(t_cub *z, int i, int j);
void				display_sprites(t_cub *z);
int					calc_sprite_hor_intersection(t_cub *z);
int					calc_sprite_ver_intersection(t_cub *z);
void				rgb_to_hexa(t_cub *z);
void				display_walls(t_cub *z);
void				classify_sprites(t_cub *z);
void				draw_sprite_pixel(void *img_ptr,
		double sprite_y, t_cub *z);
void				draw_sprite(int i, double sprite_x,
		double sprite_y, t_cub *z);
void				config_sprite_and_draw(t_cub *z, int i);
void				dist_to_sprite_1_north(t_cub *z);
void				dist_to_sprite_2_north(t_cub *z);
void				dist_to_sprite_1_east(t_cub *z);
void				dist_to_sprite_2_east(t_cub *z);
void				dist_to_sprite_1_south(t_cub *z);
void				dist_to_sprite_2_south(t_cub *z);
void				dist_to_sprite_1_west(t_cub *z);
void				dist_to_sprite_2_west(t_cub *z);
void				save_as_bmp(t_cub *z);
void				store_texture(t_cub *z);
void				store_rgb_value(int *r, int *g, int *b, char *line);
void				img_screen_ptr(t_cub *z);
int					is_correct_file(char *file);
void				ft_free_texture_ptr(t_cub *z);
void				ft_free_temp(int ***temp, int map_x);
void				ft_free_map(t_cub *z);
int					read_and_setup_config2(t_cub *z);
unsigned int		determine_color(void *img_ptr, double sprite_y, t_cub *z);
#endif
