SRCS 	= draw_sprite2.c store_map2.c config3.c calculate_wall_intersection.c calculate_sprite_intersection.c dist_to_wall.c first_intersection_coordinates.c game.c move_for_and_back.c move_right_and_left.c next_coordinates.c texture.c window.c get_next_line.c cub_utils.c cub_utils_2.c config.c config2.c conv2base.c store_map.c validate_map.c calculate_dist_to_sprite.c display_walls.c display_sprite.c image.c classify_sprites.c draw_sprite.c dist_to_sprite_north.c dist_to_sprite_east.c dist_to_sprite_south.c dist_to_sprite_west.c check_sprite.c main.c store_texture.c store_texture2.c bmp.c

OBJS	= ${SRCS:.c=.o}

MLX	= libmlx_Linux.a

LIBS	= -lX11 -lXext -lm -lbsd

NAME	= Cub3d

HEADER	= cub3d.h

CC	= gcc

RM	= rm -f

CFLAGS	= -Wall -Wextra -Werror

$(NAME): ${OBJS}
	gcc ${FLAGS} -o ${NAME} ${OBJS} ${HEADER} ${MLX} ${LIBS}

all:            ${NAME}

clean:
	${RM} ${OBJS}

fclean:         clean
	${RM} ${NAME}

re:	fclean all

.PHONY:	fclean clean all
