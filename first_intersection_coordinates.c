/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   first_intersection_coordinates.c                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: daphne <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/09 10:46:47 by daphne            #+#    #+#             */
/*   Updated: 2020/05/10 07:49:38 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	calculate_ray_y_1(t_cub *z)
{
	if (z->o_precision == 0)
	{
		if (z->o == 1 || z->o == 2)
			z->ray_y_1 = floor(z->p_y / z->side) * z->side - 1;
		else
			z->ray_y_1 = floor(z->p_y / z->side) * z->side + z->side;
	}
	else
	{
		if (z->o == 1 || z->o == 4)
			z->ray_y_1 = floor(z->p_y / z->side) * z->side - 1;
		else
			z->ray_y_1 = floor(z->p_y / z->side) * z->side + z->side;
	}
}

void	calculate_ray_x_1(t_cub *z)
{
	if (z->o_precision == 0)
	{
		if (z->o == 1)
			z->ray_x_1 = z->p_x - ((z->p_y - z->ray_y_1) * tan(z->angle));
		else if (z->o == 2)
			z->ray_x_1 = z->p_x + ((z->p_y - z->ray_y_1) / tan(z->angle));
		else if (z->o == 3)
			z->ray_x_1 = z->p_x + ((z->ray_y_1 - z->p_y) * tan(z->angle));
		else
			z->ray_x_1 = z->p_x - ((z->ray_y_1 - z->p_y) / tan(z->angle));
	}
	else
	{
		if (z->o == 1)
			z->ray_x_1 = z->p_x + (z->p_y - z->ray_y_1) * tan(z->angle);
		else if (z->o == 2)
			z->ray_x_1 = z->p_x + (z->ray_y_1 - z->p_y) / tan(z->angle);
		else if (z->o == 3)
			z->ray_x_1 = z->p_x - (z->ray_y_1 - z->p_y) * tan(z->angle);
		else
			z->ray_x_1 = z->p_x - (z->p_y - z->ray_y_1) / tan(z->angle);
	}
}

void	calculate_ray_y_2(t_cub *z)
{
	if (z->o_precision == 0)
	{
		if (z->o == 1)
			z->ray_y_2 = z->p_y - ((z->p_x - z->ray_x_2) / tan(z->angle));
		else if (z->o == 2)
			z->ray_y_2 = z->p_y - ((z->ray_x_2 - z->p_x) * tan(z->angle));
		else if (z->o == 3)
			z->ray_y_2 = z->p_y + ((z->ray_x_2 - z->p_x) / tan(z->angle));
		else
			z->ray_y_2 = z->p_y + ((z->p_x - z->ray_x_2) * tan(z->angle));
	}
	else
	{
		if (z->o == 1)
			z->ray_y_2 = z->p_y - (z->ray_x_2 - z->p_x) / tan(z->angle);
		else if (z->o == 2)
			z->ray_y_2 = z->p_y + (z->ray_x_2 - z->p_x) * tan(z->angle);
		else if (z->o == 3)
			z->ray_y_2 = z->p_y + (z->p_x - z->ray_x_2) / tan(z->angle);
		else
			z->ray_y_2 = z->p_y - (z->p_x - z->ray_x_2) * tan(z->angle);
	}
}

void	calculate_ray_x_2(t_cub *z)
{
	if (z->o_precision == 0)
	{
		if (z->o == 1 || z->o == 4)
			z->ray_x_2 = floor(z->p_x / z->side) * z->side - 1;
		else
			z->ray_x_2 = floor(z->p_x / z->side) * z->side + z->side;
	}
	else
	{
		if (z->o == 1 || z->o == 2)
			z->ray_x_2 = floor(z->p_x / z->side) * z->side + z->side;
		else
			z->ray_x_2 = floor(z->p_x / z->side) * z->side - 1;
	}
}
