/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: daphne <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/14 13:09:15 by daphne            #+#    #+#             */
/*   Updated: 2020/05/14 15:16:44 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int	exit_program(t_cub *z)
{
	int i;

	i = 0;
	if (z->save != 1)
	{
		mlx_clear_window(z->mlx, z->win);
		mlx_destroy_window(z->mlx, z->win);
	}
	while (*z->map != NULL && i < z->map_x)
	{
		free(z->map[i]);
		z->map[i] = NULL;
		i++;
	}
	z->map = NULL;
	free(z->map);
	z->map = NULL;
	free(z);
	z = NULL;
	exit(0);
	return (EXIT_SUCCESS);
}

int	ft_keyrelease(int keycode, t_cub *z)
{
	if (keycode == 0xff53)
		z->move_camera_right = 0;
	else if (keycode == 0xff51)
		z->move_camera_left = 0;
	else if (keycode == 122)
		z->move_forward = 0;
	else if (keycode == 115)
		z->move_back = 0;
	else if (keycode == 113)
		z->move_left = 0;
	else if (keycode == 100)
		z->move_right = 0;
	else if (keycode == 0xff1b)
		z->exit = 0;
	return (0);
}

int	ft_keypress(int keycode, t_cub *z)
{
	if (keycode == 0xff53)
		z->move_camera_right = 1;
	else if (keycode == 0xff51)
		z->move_camera_left = 1;
	else if (keycode == 122)
		z->move_forward = 1;
	else if (keycode == 115)
		z->move_back = 1;
	else if (keycode == 113)
		z->move_left = 1;
	else if (keycode == 100)
		z->move_right = 1;
	else if (keycode == 0xff1b)
		z->exit = 1;
	return (0);
}

int	ft_free(t_cub *z)
{
	int i;

	i = 0;
	while (i < z->map_x)
	{
		free(z->map[i]);
		z->map[i] = NULL;
		i++;
	}
	free(z->map);
	z->map = NULL;
	z->colour = NULL;
	free(z->colour);
	z->colour = NULL;
	ft_free_texture_ptr(z);
	free(z);
	z = NULL;
	return (0);
}

int	main(int argc, char **argv)
{
	t_cub *z;

	if (argc < 2)
		return (ft_error_message("Error\nMissing .cub file"));
	if (argc > 3)
		return (ft_error_message("Error\nToo many arguments"));
	if (argc == 3)
		if (!(ft_strncmp(argv[2], "--save", ft_strlen("--save"))))
			return (ft_error_message("Error\nWrong third argument"));
	if (!(z = malloc(sizeof(t_cub))))
		return (0);
	if (!(read_and_setup_config(argv[1], z)))
		return (ft_free(z));
	win_init(z, argc);
	if (!(img_init(z)))
		return (exit_program(z));
	game_init(z, argc);
	if (argc == 3)
		return (exit_program(z));
	mlx_hook(z->win, KEYPRESS_EVENT, KEYPRESS_MASK, ft_keypress, z);
	mlx_hook(z->win, KEYRELEASE_EVENT, KEYRELEASE_MASK, ft_keyrelease, z);
	mlx_hook(z->win, 17, 1L << 17, exit_program, z);
	mlx_loop_hook(z->mlx, &move, z);
	mlx_loop(z->mlx);
	return (0);
}
