/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   store_map2.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: daphne <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/22 13:05:40 by daphne            #+#    #+#             */
/*   Updated: 2020/05/22 13:07:55 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	ft_free_temp(int ***temp, int map_x)
{
	int i;

	i = 0;
	while (i < map_x)
	{
		(*temp)[i] = NULL;
		free((*temp)[i]);
		(*temp)[i] = NULL;
		i++;
	}
	free(*temp);
	*temp = NULL;
}

void	ft_free_map(t_cub *z)
{
	int i;

	i = 0;
	while (i < z->map_x)
	{
		z->map[i] = NULL;
		free(z->map[i]);
		z->map[i] = NULL;
		i++;
	}
	z->map = NULL;
	free(z->map);
	z->map = NULL;
}
