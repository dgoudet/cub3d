/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   game.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: daphne <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/09 10:49:46 by daphne            #+#    #+#             */
/*   Updated: 2020/05/14 13:37:20 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	pos_o(t_cub *z)
{
	int i;
	int j;

	i = 0;
	j = 0;
	while (i < z->map_x)
	{
		while (j < z->map_y)
		{
			if (z->map[i][j] == 3 || z->map[i][j] == 4
					|| z->map[i][j] == 5 || z->map[i][j] == 6)
			{
				z->p_x = j * z->side + z->side / 2;
				z->p_y = i * z->side + z->side / 2;
				z->o = z->map[i][j] - 2;
			}
			j++;
		}
		j = 0;
		i++;
	}
}

void	game_init(t_cub *z, int argc)
{
	z->save = 0;
	if (argc == 3)
	{
		img_screen_ptr(z);
		z->save = 1;
	}
	z->side = 64;
	z->wall_height = 64;
	z->ray_y_max = z->side * z->map_x;
	z->ray_x_max = z->side * z->map_y;
	z->fov = M_PI / 3;
	z->dist_to_proj_plane = (z->win_width / 2) / tan(z->fov / 2);
	pos_o(z);
	z->win_x = 0;
	z->angle = z->fov / 2;
	z->angle_start = z->angle;
	z->o_precision = 0;
	z->fov_angle = z->fov / 2;
	display_walls(z);
	display_sprites(z);
	if (argc == 3)
		save_as_bmp(z);
}

void	init_movement_variables(t_cub *z)
{
	z->move_speed = 20;
	z->rotation_speed = 70;
	z->fov_angle = z->fov / 2;
	z->o_precision = 0;
	z->win_x = 0;
	z->angle = z->angle_start;
	z->prev_p_x = z->p_x;
	z->prev_p_y = z->p_y;
}

int		move2(t_cub *z)
{
	if (z->move_back == 1)
	{
		move_back_config(z);
		display_walls(z);
		display_sprites(z);
	}
	else if (z->move_right == 1)
	{
		move_right_config(z);
		display_walls(z);
		display_sprites(z);
	}
	else if (z->move_left == 1)
	{
		move_left_config(z);
		display_walls(z);
		display_sprites(z);
	}
	return (0);
}

int		move(t_cub *z)
{
	init_movement_variables(z);
	if (z->exit == 1)
		exit_program(z);
	else if (z->move_camera_right == 1)
	{
		move_camera_right_config(z);
		display_walls(z);
		display_sprites(z);
	}
	else if (z->move_camera_left == 1)
	{
		move_camera_left_config(z);
		display_walls(z);
		display_sprites(z);
	}
	else if (z->move_forward == 1)
	{
		move_forward_config(z);
		display_walls(z);
		display_sprites(z);
	}
	else
		return (move2(z));
	return (0);
}
