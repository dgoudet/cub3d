/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display_walls.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: daphne <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/09 10:49:46 by daphne            #+#    #+#             */
/*   Updated: 2020/05/12 21:19:25 by daphne           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	calculate_to_put_texture(t_cub *z)
{
	calculate_horizontal_intersection(z);
	calculate_dist_to_wall_1(z);
	calculate_vertical_intersection(z);
	calculate_dist_to_wall_2(z);
	calculate_dist_to_wall(z);
	z->projected_wall_height = z->wall_height *
		z->dist_to_proj_plane / z->dist_to_wall;
	if (z->save == 1)
		store_texture(z);
	else
		put_texture(z);
}

void	display_walls(t_cub *z)
{
	if (!(z->stored_distances = malloc(sizeof(int) * z->win_width)))
		return ;
	z->count_2 = 0;
	while (z->win_x < z->win_width)
	{
		calculate_to_put_texture(z);
		if (z->win_x == z->win_width / 2 - 1)
		{
			z->angle_middle = z->angle;
			z->dist_to_wall_middle = z->dist_to_wall;
		}
		z->win_x++;
		if (z->o_precision == 0)
		{
			z->angle = z->angle - (z->fov / z->win_width);
			if (z->angle <= 0.002014)
				z->o_precision = 1;
		}
		else
			z->angle = z->angle + (z->fov / z->win_width);
		if (z->win_x < z->win_width / 2)
			z->fov_angle = z->fov_angle - (z->fov / z->win_width);
		else
			z->fov_angle = z->fov_angle + (z->fov / z->win_width);
	}
}
