/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   store_map.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: daphne <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/09 11:13:54 by daphne            #+#    #+#             */
/*   Updated: 2020/05/09 11:16:14 by daphne           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int		**map_to_temp(t_cub *z, int x, int y)
{
	int **temp;

	if (!(temp = malloc(sizeof(temp) * z->map_x)))
		return (NULL);
	while (x < z->map_x)
	{
		if (!(temp[x] = malloc(sizeof(int) * z->map_y)))
		{
			ft_free_temp(&temp, z->map_x);
			return (0);
		}
		while (y < z->map_y && z->map[x][y] >= -2 && z->map[x][y] <= 6)
		{
			temp[x][y] = z->map[x][y];
			y++;
		}
		while (y < z->map_y)
		{
			temp[x][y] = -2;
			y++;
		}
		y = 0;
		x++;
	}
	return (temp);
}

int		**temp_to_map(t_cub *z, int **temp, int *x, int y)
{
	while (*x < z->map_x)
	{
		if (!(z->map[*x] = malloc(sizeof(int) * z->map_y)))
			return (0);
		while (y < z->map_y && temp[*x][y] >= -2 && temp[*x][y] <= 6)
		{
			z->map[*x][y] = temp[*x][y];
			y++;
		}
		y = 0;
		(*x)++;
	}
	return (z->map);
}

void	add_line_to_map(t_cub *z, char *str, int x, int y)
{
	while ((str[y] >= '0' && str[y] <= '9') || str[y] == ' '
			|| str[y] == 'N' || str[y] == 'E' || str[y] == 'S' || str[y] == 'W')
	{
		if (str[y] == ' ')
			z->map[x][y] = -2;
		else if (str[y] == 'N')
			z->map[x][y] = 3;
		else if (str[y] == 'E')
			z->map[x][y] = 4;
		else if (str[y] == 'S')
			z->map[x][y] = 5;
		else if (str[y] == 'W')
			z->map[x][y] = 6;
		else if (str[y] == '0')
			z->map[x][y] = -1;
		else
			z->map[x][y] = str[y] - 48;
		y++;
	}
	while (y < z->map_y)
	{
		z->map[x][y] = -2;
		y++;
	}
}

int		return_0(int ***temp, int map_x)
{
	ft_free_temp(temp, map_x);
	return (0);
}

int		make_map(char *str, t_cub *z)
{
	int	**temp;
	int	x;
	int	y;

	x = 0;
	y = 0;
	if (z->map_x > 0)
	{
		if (!(temp = map_to_temp(z, 0, 0)))
			return (return_0(&temp, z->map_x));
		ft_free_map(z);
	}
	if (!(z->map = malloc(sizeof(temp) * z->map_x)))
		return (return_0(&temp, z->map_x));
	if (z->map_x > 0)
	{
		if (!(temp_to_map(z, temp, &x, 0)))
			return (return_0(&temp, z->map_x));
		ft_free_temp(&temp, z->map_x);
	}
	if (!(z->map[x] = malloc(sizeof(int) * z->map_y)))
		return (return_0(&temp, z->map_x));
	add_line_to_map(z, str, x, y);
	z->map_x++;
	return (1);
}
