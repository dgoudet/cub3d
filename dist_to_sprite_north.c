/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dist_to_sprite_north.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: daphne <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/09 17:37:25 by daphne            #+#    #+#             */
/*   Updated: 2020/05/10 07:48:14 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	dist_to_sprite_1_north2(t_cub *z)
{
	int			hyp1;
	int			hyp2;
	double		scal_p;
	long double	interm_angle;

	if (z->angle < M_PI / 2 / 2)
		z->dist_to_sprite_1 = sin((M_PI / 2) - z->angle_from_middle) *
			((z->p_y - z->ray_y_1) / cos(z->angle));
	else
	{
		hyp1 = (z->ray_x_1 - z->p_x) / cos(z->angle);
		hyp2 = sqrt(pow((floor(z->ray_x_1 / 64) * 64 - z->p_x), 2) +
				pow(z->ray_y_1 - z->p_y, 2));
		scal_p = 0.5 * (pow(hyp1, 2) + pow(hyp2, 2) - pow(hyp1 - hyp2, 2));
		interm_angle = acos(scal_p / (hyp1 * hyp2));
		z->dist_to_sprite_1 =
			sin((M_PI / 2) - z->angle_from_middle - interm_angle) * hyp2;
	}
}

void	dist_to_sprite_1_north(t_cub *z)
{
	int			hyp1;
	int			hyp2;
	double		scal_p;
	long double	interm_angle;

	if (z->o_precision == 0)
	{
		if (z->angle > M_PI / 2 / 2)
		{
			hyp1 = (z->p_x - z->ray_x_1) / cos(z->angle);
			hyp2 = sqrt(pow((floor(z->ray_x_1 / 64) * 64 + 64 - z->p_x), 2) +
					pow(z->ray_y_1 - z->p_y, 2));
			scal_p = 0.5 * (pow(hyp1, 2) + pow(hyp2, 2) - pow(hyp2 - hyp1, 2));
			interm_angle = acos(scal_p / (hyp1 * hyp2));
			z->dist_to_sprite_1 =
				sin((M_PI / 2) - z->angle_from_middle - interm_angle) * hyp2;
		}
		else
			z->dist_to_sprite_1 = sin((M_PI / 2) - z->angle_from_middle) *
				((z->p_y - z->ray_y_1) / cos(z->angle));
	}
	else
		dist_to_sprite_1_north2(z);
}

void	dist_to_sprite_2_north2(t_cub *z)
{
	int			hyp1;
	int			hyp2;
	double		scal_p;
	long double	interm_angle;

	if (z->angle < M_PI / 2 / 2)
	{
		hyp1 = (z->p_y - z->ray_y_2) / cos(z->angle);
		hyp2 = sqrt(pow(z->ray_x_2 - z->p_x, 2) +
				pow((floor(z->ray_y_2 / 64) * 64 + 64) - z->p_y, 2));
		scal_p = 0.5 * (pow(hyp1, 2) + pow(hyp2, 2) - pow(hyp2 - hyp1, 2));
		interm_angle = acos(scal_p / (hyp1 * hyp2));
		z->dist_to_sprite_2 =
			sin((M_PI / 2) - z->angle_from_middle - interm_angle) * hyp2;
	}
	else
		z->dist_to_sprite_2 = sin((M_PI / 2) - z->angle_from_middle) *
			((z->ray_x_2 - z->p_x) / cos(z->angle));
}

void	dist_to_sprite_2_north(t_cub *z)
{
	int			hyp1;
	int			hyp2;
	double		scal_p;
	long double	interm_angle;

	if (z->o_precision == 0)
	{
		if (z->angle > M_PI / 2 / 2)
			z->dist_to_sprite_2 = sin((M_PI / 2) - z->angle_from_middle) *
				((z->p_x - z->ray_x_2) / cos(z->angle));
		else
		{
			hyp1 = (z->p_y - z->ray_y_2) / cos(z->angle);
			hyp2 = sqrt(pow(z->ray_x_2 - z->p_x, 2) +
					pow((floor(z->ray_y_2 / 64) * 64 + 64) - z->p_y, 2));
			scal_p = 0.5 * (pow(hyp1, 2) + pow(hyp2, 2) - pow(hyp1 - hyp2, 2));
			interm_angle = acos(scal_p / (hyp1 * hyp2));
			z->dist_to_sprite_2 =
				sin((M_PI / 2) - z->angle_from_middle - interm_angle) * hyp2;
		}
	}
	else
		dist_to_sprite_2_north2(z);
}
