/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   conv2base.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: daphne <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/09 10:41:18 by daphne            #+#    #+#             */
/*   Updated: 2020/05/09 10:42:16 by daphne           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int		ft_hexatoi(char *str)
{
	int	power;
	int	i;
	int	res;

	i = 0;
	res = 0;
	while (str[i] == '0')
		i++;
	power = ft_strlen(str) - 1 - i;
	while (str[i])
	{
		if (str[i] >= '0' && str[i] <= '9')
			res = res + (str[i] - 48) * pow(16, power);
		else
			res = res + (str[i] - 87) * pow(16, power);
		power--;
		i++;
	}
	return (res);
}

void	ft_str_with_base(char *base, unsigned long nbr, t_cub *z)
{
	unsigned int	i;

	i = 0;
	while (i < nbr)
		i++;
	z->str[z->count] = base[i];
	z->count++;
}

void	ft_itoa_base(unsigned long nbr, char *base, t_cub *z)
{
	unsigned int	size;
	unsigned long	nbu;

	nbu = nbr;
	size = 0;
	size = ft_strlen(base);
	if (nbu > (size - 1))
		ft_itoa_base(nbu / size, base, z);
	ft_str_with_base(base, nbu % size, z);
}

void	ft_conv_base(unsigned long nb, char *base, t_cub *z)
{
	z->count = 0;
	if (!(z->str = malloc(sizeof(char) * (10))))
		return ;
	ft_itoa_base(nb, base, z);
	z->str[z->count] = '\0';
}
