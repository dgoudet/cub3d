/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dist_to_wall.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: daphne <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/09 10:45:32 by daphne            #+#    #+#             */
/*   Updated: 2020/05/10 07:49:15 by dgoudet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	config_dist_to_wall_1(t_cub *z)
{
	z->dist_to_wall = z->dist_to_wall_1 * cos(z->fov_angle);
	z->prev_dist = 1;
	texture_config_1(z);
}

void	config_dist_to_wall_2(t_cub *z)
{
	z->dist_to_wall = z->dist_to_wall_2 * cos(z->fov_angle);
	z->prev_dist = 2;
	texture_config_2(z);
}

void	calculate_dist_to_wall_1(t_cub *z)
{
	if (z->ray_y_1 < 0 || z->ray_y_1 >= z->ray_y_max
			|| z->ray_x_1 < 0 || z->ray_x_1 >= z->ray_x_max)
		z->dist_to_wall_1 = -1;
	else
	{
		if (z->o == 1)
			z->dist_to_wall_1 = (z->p_y - z->ray_y_1) / cos(z->angle);
		else if (z->o == 2)
			z->dist_to_wall_1 = (z->ray_x_1 - z->p_x) / cos(z->angle);
		else if (z->o == 3)
			z->dist_to_wall_1 = (z->ray_y_1 - z->p_y) / cos(z->angle);
		else
			z->dist_to_wall_1 = (z->p_x - z->ray_x_1) / cos(z->angle);
	}
}

void	calculate_dist_to_wall_2(t_cub *z)
{
	if (z->ray_y_2 < 0 || z->ray_y_2 >= z->ray_y_max
			|| z->ray_x_2 < 0 || z->ray_x_2 >= z->ray_x_max)
		z->dist_to_wall_2 = -1;
	else
	{
		if (z->o == 1)
			z->dist_to_wall_2 = (z->p_y - z->ray_y_2) / cos(z->angle);
		else if (z->o == 2)
			z->dist_to_wall_2 = (z->ray_x_2 - z->p_x) / cos(z->angle);
		else if (z->o == 3)
			z->dist_to_wall_2 = (z->ray_y_2 - z->p_y) / cos(z->angle);
		else
			z->dist_to_wall_2 = (z->p_x - z->ray_x_2) / cos(z->angle);
	}
}

void	calculate_dist_to_wall(t_cub *z)
{
	if (z->dist_to_wall_1 < 0)
		config_dist_to_wall_2(z);
	else if (z->dist_to_wall_2 < 0)
		config_dist_to_wall_1(z);
	else if (((z->dist_to_wall_1 - z->dist_to_wall_2) >= 0
				&& (z->dist_to_wall_1 - z->dist_to_wall_2) <= 10)
			|| ((z->dist_to_wall_2 - z->dist_to_wall_1) >= 0
				&& (z->dist_to_wall_2 - z->dist_to_wall_1) <= 10))
	{
		if (z->prev_dist == 1)
			config_dist_to_wall_1(z);
		else
			config_dist_to_wall_2(z);
	}
	else if (z->dist_to_wall_1 < z->dist_to_wall_2)
		config_dist_to_wall_1(z);
	else
		config_dist_to_wall_2(z);
	z->stored_distances[z->count_2] = z->dist_to_wall;
	z->count_2++;
}
