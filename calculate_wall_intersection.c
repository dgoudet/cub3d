/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   calculate_wall_intersection.c                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dgoudet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/08 19:51:43 by dgoudet           #+#    #+#             */
/*   Updated: 2020/05/09 10:14:34 by daphne           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	calculate_horizontal_intersection(t_cub *z)
{
	int	i;
	int	j;

	i = 0;
	j = 0;
	calculate_ray_y_1(z);
	calculate_ray_x_1(z);
	i = z->ray_y_1 / z->side;
	j = z->ray_x_1 / z->side;
	if ((z->ray_y_1 >= 0 && z->ray_y_1 <= z->ray_y_max)
			&& (z->ray_x_1 >= 0 && z->ray_x_1 <= z->ray_x_max))
		while (z->map[i][j] != 1
				&& (z->ray_y_1 >= 0 && z->ray_y_1 <= z->ray_y_max)
				&& (z->ray_x_1 >= 0 && z->ray_x_1 <= z->ray_x_max))
		{
			calculate_n_ray_y_1(z);
			calculate_n_ray_x_1(z);
			if ((z->ray_y_1 > 0 && z->ray_y_1 < z->ray_y_max)
					&& (z->ray_x_1 > 0 && z->ray_x_1 < z->ray_x_max))
			{
				i = z->ray_y_1 / z->side;
				j = z->ray_x_1 / z->side;
			}
		}
}

void	calculate_vertical_intersection(t_cub *z)
{
	int	i;
	int	j;

	i = 0;
	j = 0;
	calculate_ray_x_2(z);
	calculate_ray_y_2(z);
	i = z->ray_y_2 / z->side;
	j = z->ray_x_2 / z->side;
	if ((z->ray_y_2 >= 0 && z->ray_y_2 <= z->ray_y_max)
			&& (z->ray_x_2 >= 0 && z->ray_x_2 <= z->ray_x_max))
		while (z->map[i][j] != 1
				&& (z->ray_y_2 >= 0 && z->ray_y_2 <= z->ray_y_max)
				&& (z->ray_x_2 >= 0 && z->ray_x_2 <= z->ray_x_max))
		{
			calculate_n_ray_x_2(z);
			calculate_n_ray_y_2(z);
			if ((z->ray_y_2 > 0 && z->ray_y_2 < z->ray_y_max)
					&& (z->ray_x_2 > 0 && z->ray_x_2 < z->ray_x_max))
			{
				i = z->ray_y_2 / z->side;
				j = z->ray_x_2 / z->side;
			}
		}
}
